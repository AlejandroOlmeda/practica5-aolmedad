package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ImagesDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<ImagesGame> imagesGames);

    @Query("SELECT * FROM images_games")
    public List<ImagesGame> getAll();

    @Insert
    public long insert(ImagesGame imagesGame);

    @Query("DELETE FROM images_games")
    public void deleteAll();

    @Query("SELECT * FROM images_games WHERE id = :id")
    public List<ImagesGame> getByID(Integer id);
/*
    @Query("SELECT count(id) FROM popular_games")
    public Integer getNumberOfPopularGames();
*/
}
