package es.unex.giiis.gb02project.dataApp.network;

import es.unex.giiis.gb02project.dataApp.model.*;

public interface OnConsultaLoadedListener {
    public void onConsultaLoaded(Consulta consulta);
}