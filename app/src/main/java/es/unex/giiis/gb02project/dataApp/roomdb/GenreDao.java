package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.Genre;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface GenreDao {
    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Genre> genres);

    @Query("SELECT * FROM genre_list")
    public List<Genre> getAll();

    @Query("SELECT * FROM genre_list")
    public LiveData<List<Genre>> getLiveData();

    @Insert
    public void insert(Genre genres);

    @Query("DELETE FROM genre_list")
    public void deleteAll();

    @Update
    public int update(Genre genres);
}
