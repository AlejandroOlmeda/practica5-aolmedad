package es.unex.giiis.gb02project.ui.explora;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;

public class ExploraViewModel extends ViewModel {

    private final ExploraRepository mRepository;
    private final LiveData<List<PopularGames>> mPopularGames;
    private final LiveData<List<NewGames>> mNewGames;
    private final LiveData<List<Platform>> mPlatforms;
    private final LiveData<List<Result>> mSearchGames;
    private  Map<String, String> mParams = new HashMap<>();
    private Integer mIdgame;


    public ExploraViewModel(ExploraRepository exploraRepository) {
        mRepository = exploraRepository;
        mPopularGames = mRepository.getCurrentPopular();
        mNewGames = mRepository.getCurrentNew();
        mPlatforms = mRepository.getCurrentPlatforms();
        mSearchGames = mRepository.getCurrentSearchGames();
    }

    public void setParams(Map<String, String> params, int tipo){
        mParams = params;
        if(mParams.size() == 0 && tipo == 0){
            mRepository.getPopularGames(params);
        }
        else if (mParams.size() != 0 && tipo == 0){
            mRepository.getNewGames(params);
        }
        else {
            mRepository.getSearchGames(params);
        }
    }


    public void onRefresh() {
        mRepository.doFetchPopular(mParams,null);
    }

    public LiveData<List<PopularGames>> getPopular() {
        return mPopularGames;
    }

    public LiveData<List<NewGames>> getNew() {
        return mNewGames;
    }

    public LiveData<List<Result>> getSearchGames() {
        return mSearchGames;
    }

    public void setIDGame(Integer idgame){
        mIdgame = idgame;
        mRepository.setGame(idgame);
    }

    public LiveData<List<Platform>> getPlatform() {
        return mPlatforms;
    }


}