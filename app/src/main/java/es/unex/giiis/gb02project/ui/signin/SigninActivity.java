package es.unex.giiis.gb02project.ui.signin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.ui.editAccount.EditAccountViewModel;

public class SigninActivity extends AppCompatActivity {

    private static final int ADD_TODO_ITEM_REQUEST = 0;
    private SigninViewModel signinViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        signinViewModel = new ViewModelProvider(this, appContainer.signinViewModelFactory).get(SigninViewModel.class);

        final EditText registerUsername = findViewById(R.id.RegisterUsername);
        final EditText registerEmail = findViewById(R.id.RegisterEmail);
        final EditText registerPassword = findViewById(R.id.RegisterPassword);
        final EditText registerConfirmPassword = findViewById(R.id.RegisterConfirmPassword);
        final Button registerButton = findViewById(R.id.SendRegister);

        final Context context = this;
        final LifecycleOwner lifecycleOwner = this;

        final Boolean[] added = {false};

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                if((registerPassword.getText().toString()).equals(registerConfirmPassword.getText().toString())) {
                    if((registerPassword.getText().toString()).length() > 5) {
                        signinViewModel.getUserByUsernameLiveData(registerUsername.getText().toString()).observe(lifecycleOwner, new Observer<User>() {
                            @Override
                            public void onChanged(User user) {
                                if (user == null) {
                                    User.packageIntent(intent, registerUsername.getText().toString(), registerEmail.getText().toString(), registerPassword.getText().toString());
                                    final User newUser = new User(intent);
                                    signinViewModel.insert(newUser);
                                    added[0] = true;

                                } else if (!added[0]){
                                    Toast toast = Toast.makeText(context, "Usuario no valido", Toast.LENGTH_LONG);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                } else if(added[0]){
                                    SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                                    sp.edit().putLong("UserID", user.getID()).commit();
                                    sp.edit().putString("Username", registerUsername.getText().toString()).commit();

                                    finish();
                                }
                            }
                        });
                    }else{
                        Toast toast = Toast.makeText(context,"La contraseña debe tener al menos 6 caracteres", Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER, 0,0);
                        toast.show();
                    }
                }else{
                    Toast toast = Toast.makeText(context,"Las contraseñas no coinciden", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0,0);
                    toast.show();
                }
            }
        });
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}