package es.unex.giiis.gb02project.ui.login;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import es.unex.giiis.gb02project.data.LoginDataSource;
import es.unex.giiis.gb02project.data.LoginRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
public class LoginViewModelFactory implements ViewModelProvider.Factory {

    private final PerfilRepository mRepository;

    public LoginViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(LoginRepository.getInstance(new LoginDataSource()), mRepository);
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}