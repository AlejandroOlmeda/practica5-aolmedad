package es.unex.giiis.gb02project.ui.gamedetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersRating;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

public class GameDetailsViewModel extends ViewModel {
    private final PerfilRepository mRepository;
    private final LiveData<List<UsersCompleted>> mUsersCompleted;
    private final LiveData<List<UsersWishlist>> mUsersWishlist;
    private final LiveData<List<UsersPlaying>> mUsersPlaying;
    private final LiveData<List<Platform>> mPlatforms;
    private final LiveData<List<ImagesGame>> mImages;
    private long midUser;

    public GameDetailsViewModel(PerfilRepository perfilRepository) {
        mRepository = perfilRepository;
        mUsersCompleted = mRepository.getCurrentUsersCompleted();
        mUsersWishlist = mRepository.getCurrentUsersWishlist();
        mUsersPlaying = mRepository.getCurrentUsersPlaying();
        mPlatforms = mRepository.getCurrentPlatforms();
        mImages = mRepository.getCurrentImages();
    }

    public void setIdUser(long idUser){
        midUser = idUser;
        mRepository.setUser(idUser);
    }

    public void insertCompleted(UsersCompleted usersCompleted){
        mRepository.insertCompleted(usersCompleted);
    }

    public void deleteCompleted(long idUser, Integer idGame){
        mRepository.deleteCompleted(idUser, idGame);
    }

    public void insertWishlist(UsersWishlist usersWishlist){
        mRepository.insertWishlist(usersWishlist);
    }

    public void deleteWishlist(long idUser, Integer idGame){
        mRepository.deleteWishlist(idUser, idGame);
    }

    public void insertPlaying(UsersPlaying usersPlaying){
        mRepository.insertPlaying(usersPlaying);
    }

    public void deletePlaying(long idUser, Integer idGame){
        mRepository.deletePlaying(idUser, idGame);
    }

    public void insertOrUpdateRating(UsersRating usersRating){
        mRepository.insertOrUpdateRating(usersRating);
    }

    public LiveData<UsersRating> getRatingGameUser(long iduser, long idgame){
        return mRepository.getRatingGameUser(iduser, idgame);
    }

    public LiveData<List<UsersCompleted>> getCompleted(){
        return mUsersCompleted;
    }

    public LiveData<List<UsersWishlist>> getWishlist(){
        return mUsersWishlist;
    }

    public LiveData<List<UsersPlaying>> getPlaying(){
        return mUsersPlaying;
    }

    public LiveData<List<Platform>> getPlatforms(){
        return mPlatforms;
    }

    public LiveData<List<ImagesGame>> getImages(){
        return mImages;
    }

}
