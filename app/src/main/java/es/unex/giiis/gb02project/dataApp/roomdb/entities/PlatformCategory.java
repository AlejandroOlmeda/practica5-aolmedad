package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "platform_category")
public class PlatformCategory implements Serializable {

    @Ignore
    public final static String IDPlatform = "IDPlatform";
    @Ignore
    public final static String NAME = "name";


    @SerializedName("mIDPlatform")
    @PrimaryKey
    @ColumnInfo(name = "IDPlatform")
    private Integer mIDPlatform;

    @SerializedName("mName")
    @ColumnInfo(name = "name")
    private String mName = new String();


    public PlatformCategory(Integer mIDPlatform, String mName) {
        this.mIDPlatform = mIDPlatform;
        this.mName = mName;
    }


    public Integer getIDPlatform() {
        return mIDPlatform;
    }

    public void setIDPlatform(Integer mIDPlatform) {
        this.mIDPlatform = mIDPlatform;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public static void packageIntent(Intent intent, Integer mIDPlatform, String mName) {
        intent.putExtra(PlatformCategory.IDPlatform, mIDPlatform);
        intent.putExtra(PlatformCategory.NAME, mName);
    }

    @Ignore
    public PlatformCategory(Intent intent) {
        mIDPlatform = intent.getIntExtra(PlatformCategory.IDPlatform, 0);
        mName = intent.getStringExtra(PlatformCategory.NAME);
    }

}
