package es.unex.giiis.gb02project.dataApp.network;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import es.unex.giiis.gb02project.AppExecutors;
import es.unex.giiis.gb02project.dataApp.model.Consulta;
import es.unex.giiis.gb02project.dataApp.model.Result;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResultNetworkLoaderRunnable implements Runnable {
    private final OnResultLoadedListener mOnResultLoadedListener;
    private final List<Long> mCompletados;
    private final Map<String, String> mParametros;

    public ResultNetworkLoaderRunnable(Map<String, String> parametros, List<Long> completados, OnResultLoadedListener onResultLoadedListener){
        mOnResultLoadedListener = onResultLoadedListener;
        mCompletados = completados;
        mParametros = parametros;
    }

    @Override
    public void run() {
        // Instanciación de Retrofit y llamada síncrona
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.rawg.io/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RawgService service = retrofit.create(RawgService.class);
        try {
            List<Result> gResults = new ArrayList<>();
            if(mParametros == null) {
                for (Long uc : mCompletados) {
                    Result result = service.getConsultaJuegoID(uc.toString()).execute().body();
                    gResults.add(result);
                }
            }else{
                mParametros.put("page", "1");
                for(int i=1; i<=10; i++){
                    mParametros.replace("page", Integer.toString(i));
                    Consulta consulta = service.getConsultaParametros(mParametros).execute().body();
                    if(consulta != null) {
                        for (Result r : consulta.getResults()) {
                            gResults.add(r);
                        }
                    }
                }
            }
            AppExecutors.getInstance().mainThread().execute(() -> mOnResultLoadedListener.onResultLoaded(gResults));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Llamada al Listener con los datos obtenidos
    }
}
