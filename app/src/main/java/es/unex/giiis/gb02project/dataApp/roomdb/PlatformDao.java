package es.unex.giiis.gb02project.dataApp.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PlatformDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Platform> Platform);

    @Query("SELECT * FROM platform")
    public List<Platform> getAll();

    @Query("SELECT * FROM platform WHERE IDGame = :idgame")
    public List<Platform> getPlatformByGame(Integer idgame);


    @Query("SELECT * FROM platform WHERE IDGame = :idgame ")
    public LiveData<List<Platform>> getLiveData(Integer idgame);

    @Insert
    public void insert(Platform platform);

    @Query("DELETE FROM platform")
    public void deleteAll();

    @Query("DELETE FROM platform WHERE type = :Type")
    public void deleteType(int Type);

    @Update
    public int update(Platform platform);

   /* @Query("DELETE FROM platform WHERE IDPlatform = :idplatform AND IDGame = :idgame")
    public void deleteGameFrom(long idplatform, long idgame);*/
}
