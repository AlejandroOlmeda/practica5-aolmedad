package es.unex.giiis.gb02project.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import es.unex.giiis.gb02project.R;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {
    private List<String> mDataset;
    private Context mContext;



    public interface OnListInteractionListener{
        public void onListInteraction(String url);
    }

    public ImagesAdapter.OnListInteractionListener mListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case

        public ImageView mImageView;
        public View mView;
        public String mItem;

        public MyViewHolder(View v) {
            super(v);
            mView=v;
            mImageView = v.findViewById(R.id.game_gallery);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ImagesAdapter(List<String> myDataset, ImagesAdapter.OnListInteractionListener listener, Context context){
        mDataset = myDataset;
        mListener = listener;
        mContext = context;
    }

    @Override
    public ImagesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        // Create new views (invoked by the layout manager)
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_image_view, parent, false);

        return new ImagesAdapter.MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ImagesAdapter.MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);

        RequestOptions requestOptions = new RequestOptions().centerCrop();

        Glide.with(mContext)
                .load(mDataset.get(position))
                .apply(requestOptions)
                .placeholder(R.drawable.loading)
                .into(holder.mImageView);


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListInteraction((holder.mItem));
                    //mListener.onListInteraction(holder.mItem.getBackgroundImage());
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void swap(List<String> dataset){
        mDataset = dataset;
        notifyDataSetChanged();
    }
}
