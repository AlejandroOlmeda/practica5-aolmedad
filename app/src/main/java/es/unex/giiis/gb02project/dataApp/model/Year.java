
package es.unex.giiis.gb02project.dataApp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Year implements Serializable {

    @SerializedName("from")
    @Expose
    private Integer from;
    @SerializedName("to")
    @Expose
    private Integer to;
    @SerializedName("filter")
    @Expose
    private String filter;
    @SerializedName("decade")
    @Expose
    private Integer decade;
    @SerializedName("years")
    @Expose
    private List<Year_> years = null;
    @SerializedName("nofollow")
    @Expose
    private Boolean nofollow;
    @SerializedName("count")
    @Expose
    private Integer count;

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    public Integer getTo() {
        return to;
    }

    public void setTo(Integer to) {
        this.to = to;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Integer getDecade() {
        return decade;
    }

    public void setDecade(Integer decade) {
        this.decade = decade;
    }

    public List<Year_> getYears() {
        return years;
    }

    public void setYears(List<Year_> years) {
        this.years = years;
    }

    public Boolean getNofollow() {
        return nofollow;
    }

    public void setNofollow(Boolean nofollow) {
        this.nofollow = nofollow;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
