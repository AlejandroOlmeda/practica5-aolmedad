package es.unex.giiis.gb02project.dataApp.roomdb.entities;

import android.content.Intent;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "genre_list")
public class Genre implements Serializable {
    @Ignore
    public final static String IDGenre = "IDGenre";
    @Ignore
    public final static String NAME = "name";


    @SerializedName("mIDGenre")
    @PrimaryKey
    @ColumnInfo(name = "IDGenre")
    private Integer mIDGenre;

    @SerializedName("mName")
    @ColumnInfo(name = "name")
    private String mName = new String();


    public Genre(Integer mIDGenre, String mName) {
        this.mIDGenre = mIDGenre;
        this.mName = mName;
    }


    public Integer getIDGenre() {
        return mIDGenre;
    }

    public void setIDGenre(Integer mIDGenre) {
        this.mIDGenre = mIDGenre;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public static void packageIntent(Intent intent, Integer mIDGenre, String mName) {
        intent.putExtra(Genre.IDGenre, mIDGenre);
        intent.putExtra(Genre.NAME, mName);
    }

    @Ignore
    public Genre(Intent intent) {
        mIDGenre = intent.getIntExtra(Genre.IDGenre, 0);
        mName = intent.getStringExtra(Genre.NAME);
    }
}
