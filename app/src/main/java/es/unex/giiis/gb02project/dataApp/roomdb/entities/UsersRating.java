package es.unex.giiis.gb02project.dataApp.roomdb.entities;


import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "users_rating", primaryKeys = {"IDUser", "IDGame"})
public class UsersRating {
    @Ignore
    public static final String ITEM_SEP = System.getProperty("line.separator");

    @Ignore
    public final static String IDUser = "IDUser";
    @Ignore
    public final static String IDGame = "IDGame";
    @Ignore
    public final static String Rating = "Rating";

    @ColumnInfo(name = "IDUser") @NonNull
    private long mIDUser;
    @ColumnInfo(name = "IDGame") @NonNull
    private long mIDGame;
    @ColumnInfo(name = "Rating") @NonNull
    private float mRating;


    public UsersRating(long mIDUser, long mIDGame, float mRating) {
        this.mIDUser = mIDUser;
        this.mIDGame = mIDGame;
        this.mRating = mRating;
    }


    // Create a new User from data packaged in an Intent

    @Ignore
    UsersRating(Intent intent) {
        mIDUser = intent.getLongExtra(UsersRating.IDUser,0); mIDGame = intent.getLongExtra(UsersRating.IDGame, 0);
        mIDGame = intent.getLongExtra(UsersRating.IDGame, 0);
        mRating = intent.getIntExtra(UsersRating.Rating, 0);
    }

    public long getIDUser() { return mIDUser; }

    public void setIDUser(long IDUser) { this.mIDUser = IDUser; }

    public long getIDGame() { return mIDGame; }

    public void setIDGame(long IDGame) { this.mIDGame = IDGame; }

    public float getRating() { return mRating; }

    public void setRating(float Rating) { this.mRating = Rating; }


    // Take a set of String data values and
    // package them for transport in an Intent

    public static void packageIntent(Intent intent, long IDUser, long IDGame, float Rating) {
        intent.putExtra(UsersRating.IDUser, IDUser);
        intent.putExtra(UsersRating.IDGame, IDGame);
        intent.putExtra(UsersRating.Rating, Rating);
    }

}
