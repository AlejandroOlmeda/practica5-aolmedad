package es.unex.giiis.gb02project.dataApp.network;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.Result;

public interface OnResultLoadedListener {
    public void onResultLoaded(List<Result> results);
}
