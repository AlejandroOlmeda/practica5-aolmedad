package es.unex.giiis.gb02project.ui.perfil;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.dataApp.roomdb.CustomGamesDao;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersPlaying;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersWishlist;

public class PerfilViewModel extends ViewModel {

    private final PerfilRepository mRepository;
    private final LiveData<List<UsersCompleted>> mUsersCompleted;
    private final LiveData<List<UsersWishlist>> mUsersWishlist;
    private final LiveData<List<UsersPlaying>> mUsersPlaying;
    private final LiveData<List<CustomGame>>  mUsersCustom;
    private long midUser;

    public PerfilViewModel(PerfilRepository perfilRepository) {
        mRepository = perfilRepository;
        mUsersCompleted = mRepository.getCurrentUsersCompleted();
        mUsersWishlist = mRepository.getCurrentUsersWishlist();
        mUsersPlaying = mRepository.getCurrentUsersPlaying();
        mUsersCustom = mRepository.getCurrentUsersCustom();
    }

    public void setIdUser(long idUser){
        midUser = idUser;
        mRepository.setUser(idUser);
    }



    public LiveData<List<UsersCompleted>> getCompleted(){
        return mUsersCompleted;
    }

    public LiveData<List<UsersWishlist>> getWishlist(){
        return mUsersWishlist;
    }

    public LiveData<List<UsersPlaying>> getPlaying(){
        return mUsersPlaying;
    }

    public LiveData<List<CustomGame>> getCustom(){
        return mUsersCustom;
    }

}