package es.unex.giiis.gb02project.ui.editAccount;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.unex.giiis.gb02project.AppContainer;
import es.unex.giiis.gb02project.MyApplication;
import es.unex.giiis.gb02project.R;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;

public class EditAccountActivity extends AppCompatActivity {

    private EditAccountViewModel editAccountViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        final TextView username = findViewById(R.id.username_box);
        final TextView email = findViewById(R.id.usermail_box);
        final TextView oldpassword = findViewById(R.id.oldpassword_box);
        final TextView newpassword = findViewById(R.id.newpassword_box);
        final TextView confirmnewpassword = findViewById(R.id.confirmnewpassword_box);
        final Button buttonConfirm = findViewById(R.id.editcuenta_button);
        final Context context = this;
        final LifecycleOwner lifecycleOwner = this;

        AppContainer appContainer = ((MyApplication) getApplication()).appContainer;
        editAccountViewModel = new ViewModelProvider(this, appContainer.editAccountViewModelFactory).get(EditAccountViewModel.class);

        final SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        final User[] currentUser = new User[1];

        String name = sp.getString("Username", "");

        editAccountViewModel.getUserByUsernameLiveData(name).observe(lifecycleOwner, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if(user!=null) {
                    username.setText(user.getUsername());
                    email.setText(user.getEmail());
                    currentUser[0] = user;
                }
            }
        });

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] toastmsg = {""};
                final boolean[] validUpdate = {true};

                editAccountViewModel.getUserByUsernameLiveData(username.getText().toString()).observe(lifecycleOwner, new Observer<User>() {
                    @Override
                    public void onChanged(User userOtro) {
                        User editedUser = currentUser[0];
                        //Si se ha introducido un nuevo nombre de usuario
                        if(!username.getText().toString().equals(sp.getString("Username", ""))) {
                            //si no existe otro usuario con ese nombre, se podrá cambiar
                            if (userOtro == null) {
                                //poder cambiarlo
                                validUpdate[0] = true;
                                editedUser.setUsername(username.getText().toString());
                            } else {//si ya existe otro usuario con ese nombre no se podrá cambiar
                                //mostrar toast de usuario no valido
                                validUpdate[0] = false;
                                toastmsg[0] = toastmsg[0] + "Usuario no valido";
                            }
                        }
                        //si se quiere actualizar la contraseña (la nueva contraseña no esta vacia
                        if(!newpassword.getText().toString().isEmpty()){
                            //Si las dos nuevas contraseñas coinciden y además tienen mas de 5 caracteres se podrá añadir
                            if(validUpdate[0] && newpassword.getText().toString().equals(confirmnewpassword.getText().toString()) && newpassword.getText().toString().length()>5){
                                //Si la vieja contraseña es correcta
                                if(oldpassword.getText().toString().equals(currentUser[0].getPassword())){
                                    //se puede cambiar
                                    validUpdate[0] = true;
                                    editedUser.setPassword(newpassword.getText().toString());
                                }else{
                                    //vieja contraseña no valida
                                    validUpdate[0] = false;
                                    toastmsg[0] = toastmsg[0] + "\nAntigua contraseña no valida";
                                }
                            }else{//Si la nueva contraseña no es valida
                                //mostrar toast de nueva contraseña no valida. tienen que ser iguales y de mas de 6 char
                                validUpdate[0] = false;
                                toastmsg[0] = toastmsg[0] + "\nNueva contraseña no valida";
                            }
                        }

                        if(!email.getText().toString().isEmpty()){
                            editedUser.setEmail(email.getText().toString());
                        }
                        else{
                            validUpdate[0] = false;
                            toastmsg[0] = toastmsg[0] + "\nEmail no válido";
                        }

                        if (validUpdate[0]) {
                            //Actualizar usuario en la base de datos
                            editAccountViewModel.updateUser(editedUser);
                            SharedPreferences sp = getSharedPreferences("MyPref", Context.MODE_PRIVATE);
                            sp.edit().putLong("UserID", editedUser.getID()).commit();
                            sp.edit().putString("Username", editedUser.getUsername()).commit();
                            finish();
                        } else {
                            Toast toast = Toast.makeText(context, toastmsg[0], Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0, 0);
                                toast.show();

                        }
                    }
                });






            }

        });

    }


}