package es.unex.giiis.gb02project.dataApp.roomdb;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.CustomGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.UsersCompleted;

@Dao
public interface CustomGamesDao {
    @Query("SELECT * FROM custom_games")
    public List<CustomGame> getAll();

    @Query("SELECT * FROM custom_games WHERE IDUser = :iduser")
    public List<CustomGame> getGamesByUser(long iduser);


    @Query("SELECT * FROM custom_games WHERE IDUser = :iduser ")
    public LiveData<List<CustomGame>> getLiveData(long iduser);

    @Insert
    public void insert(CustomGame customGame);

    @Query("DELETE FROM custom_games")
    public void deleteAll();

    @Update
    public int update(CustomGame customGame);

    @Query("DELETE FROM custom_games WHERE IDUser = :iduser AND IDGame = :idgame")
    public void deleteGameFromCustom(long iduser, long idgame);
}
