package es.unex.giiis.gb02project.ui.perfil;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import es.unex.giiis.gb02project.dataApp.ExploraRepository;
import es.unex.giiis.gb02project.dataApp.PerfilRepository;
import es.unex.giiis.gb02project.ui.explora.ExploraViewModel;

public class PerfilViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final PerfilRepository mRepository;

    public PerfilViewModelFactory(PerfilRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new PerfilViewModel(mRepository);
    }
}
