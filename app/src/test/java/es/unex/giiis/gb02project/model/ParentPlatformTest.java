package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.ParentPlatform;
import es.unex.giiis.gb02project.dataApp.model.Platform__;

import static org.junit.Assert.*;

public class ParentPlatformTest {

    private ParentPlatform instance;

    @Before
    public void setUp() throws Exception {
        instance = new ParentPlatform();
    }

    @Test
    public void getPlatform() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("platform");
        field.setAccessible(true);
        Platform__ value = new Platform__();
        field.set(instance, value);
        //when
        final Platform__ result = instance.getPlatform();
        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPlatform() throws IllegalAccessException, NoSuchFieldException {
        Platform__ value = new Platform__();
        instance.setPlatform(value);
        final Field field = instance.getClass().getDeclaredField("platform");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}