package es.unex.giiis.gb02project.entities;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.roomdb.entities.User;

import static org.junit.Assert.*;

public class UserTest {

    @Test
    public void getID() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "Jaime", "jlopesan", "hola");
        final Field field = instance.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        field.set(instance, new Long(1));
        final Long result = instance.getID();
        assertEquals("Los campos no coinciden", result, new Long(1));
    }

    @Test
    public void setID() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "Jaime", "jlopesan", "hola");
         Long value = new Long(1) ;
        instance.setID(value);
        final Field field = instance.getClass().getDeclaredField("mID");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getUsername() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "Jaime", "jlopesan", "hola");
        final Field field = instance.getClass().getDeclaredField("mUsername");
        field.setAccessible(true);
        field.set(instance, "Jaime");
        final String result = instance.getUsername();
        assertEquals("Los campos no coinciden", result, "Jaime");
    }

    @Test
    public void setUsername() throws NoSuchFieldException, IllegalAccessException{

        final User instance = new User(1, "ExampleName", "ExampleEmail", "ExamplePassword");
        String value = "ExampleName";
        instance.setUsername(value);
        final Field field = instance.getClass().getDeclaredField("mUsername");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getEmail() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "Jaime", "jlopesan", "hola");
        final Field field = instance.getClass().getDeclaredField("mEmail");
        field.setAccessible(true);
        field.set(instance, "jlopesan");
        final String result = instance.getEmail();
        assertEquals("Los campos no coinciden", result, "jlopesan");
    }

    @Test
    public void setEmail() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "Jaime", "jlopesan", "hola");
        String value = "jlopesan";
        instance.setEmail(value);
        final Field field = instance.getClass().getDeclaredField("mEmail");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getPassword() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "UserExample", "EmailExample", "PasswordExample");
        final Field field = instance.getClass().getDeclaredField("mPassword");
        field.setAccessible(true);
        field.set(instance, "PasswordExample");
        final String result = instance.getPassword();
        assertEquals("Los campos no coinciden", result, "PasswordExample");
    }

    @Test
    public void setPassword() throws NoSuchFieldException, IllegalAccessException{
        final User instance = new User(1, "UserExample", "EmailExample", "PasswordExample");
        String value = "PasswordExample";
        instance.setPassword(value);
        final Field field = instance.getClass().getDeclaredField("mPassword");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

}