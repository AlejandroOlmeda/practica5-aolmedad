package es.unex.giiis.gb02project.network;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

import es.unex.giiis.gb02project.dataApp.model.Consulta;
import es.unex.giiis.gb02project.dataApp.model.Platform;
import es.unex.giiis.gb02project.dataApp.model.Platform_;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.network.RawgService;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RawgServiceTest {

    private RawgService rawgService;
    private MockWebServer mockWebServer;
    private Result result1;
    private Result result2;

    @Before
    public void setUp() throws Exception {
        mockWebServer = new MockWebServer();

        result1 = new Result();
        result1.setId(1);
        result1.setName("Juego de prueba");
        result1.setDescription("Descripción de prueba");
        result1.setRating(4.2);

        Platform p1 = new Platform();
        Platform_ p_1 = new Platform_();
        p_1.setId(4);
        p_1.setName("Aventura");
        p1.setPlatform(p_1);
        List<Platform> lp = new ArrayList<>();
        lp.add(p1);
        result1.setPlatforms(lp);



        result2 = new Result();
        result2.setId(2);
        result2.setName("Juego de prueba 2");
        result2.setDescription("Descripción de prueba 2");
        result2.setRating(4.2);
        Platform p2 = new Platform();
        Platform_ p_2 = new Platform_();
        p_2.setId(5);
        p_2.setName("Acción");
        p2.setPlatform(p_2);
        List<Platform> lp2 = new ArrayList<>();
        lp2.add(p2);
        result2.setPlatforms(lp2);
    }

    @After
    public void tearDown() throws Exception {
        mockWebServer.shutdown();
    }


    @Test
    public void getConsultaGeneros() throws IOException {

        Result genre1 = new Result();
        genre1.setId(1);
        genre1.setName("Primer genero");


        Result genre2 = new Result();
        genre2.setId(2);
        genre2.setName("Segundo genero");

        List<Result> lr = new ArrayList<>();

        lr.add(genre1);
        lr.add(genre2);

        Consulta consulta = new Consulta();
        consulta.setResults(lr);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(consulta));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        RawgService service = retrofit.create(RawgService.class);

        //Step6: we create the call to get centers
        Call<Consulta> call = service.getConsultaGeneros();
        // and we execute the call
        Response<Consulta> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Result> resultsResponse =response.body().getResults();
        assertFalse(resultsResponse.isEmpty());
        assertTrue(resultsResponse.size()==2);
        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert

        assertEquals(resultsResponse.get(0).getId(), genre1.getId());
        assertEquals(resultsResponse.get(0).getName(), genre1.getName());

        assertEquals(resultsResponse.get(1).getId(), genre2.getId());
        assertEquals(resultsResponse.get(1).getName(), genre2.getName());

    }

    @Test
    public void getConsultaParametros() throws IOException {
        List<Result> lr = new ArrayList<>();

        lr.add(result1);
        lr.add(result2);

        Consulta consulta = new Consulta();
        consulta.setResults(lr);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(consulta));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        RawgService service = retrofit.create(RawgService.class);

        Map<String,String> map = new HashMap<>();
        map.put("genre", "1");
        map.put("search", "prueba");

        //Step6: we create the call to get centers
        Call<Consulta> call = service.getConsultaParametros(map);
        // and we execute the call
        Response<Consulta> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Result> resultsResponse =response.body().getResults();
        assertFalse(resultsResponse.isEmpty());
        assertTrue(resultsResponse.size()==2);
        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert

        assertEquals(resultsResponse.get(0).getId(), result1.getId());
        assertEquals(resultsResponse.get(0).getName(), result1.getName());
        assertEquals(resultsResponse.get(0).getDescription(), result1.getDescription());
        assertEquals(resultsResponse.get(0).getRating(), result1.getRating());
        assertTrue(resultsResponse.get(0).getPlatforms().size()== result1.getPlatforms().size());
        assertEquals(resultsResponse.get(0).getPlatforms().get(0).getPlatform().getId(), result1.getPlatforms().get(0).getPlatform().getId());
        assertEquals(resultsResponse.get(0).getPlatforms().get(0).getPlatform().getName(), result1.getPlatforms().get(0).getPlatform().getName());

        assertEquals(resultsResponse.get(1).getId(), result2.getId());
        assertEquals(resultsResponse.get(1).getName(), result2.getName());
        assertEquals(resultsResponse.get(1).getDescription(), result2.getDescription());
        assertEquals(resultsResponse.get(1).getRating(), result2.getRating());
        assertTrue(resultsResponse.get(1).getPlatforms().size()==result2.getPlatforms().size());
        assertEquals(resultsResponse.get(1).getPlatforms().get(0).getPlatform().getId(), result2.getPlatforms().get(0).getPlatform().getId());
        assertEquals(resultsResponse.get(1).getPlatforms().get(0).getPlatform().getName(), result2.getPlatforms().get(0).getPlatform().getName());
    }

    @Test
    public void getConsultaPlataformas() throws IOException {

        Result platform1 = new Result();
        platform1.setId(1);
        platform1.setName("Primera plataforma");


        Result platform2 = new Result();
        platform2.setId(2);
        platform2.setName("Segunda plataforma");

        List<Result> lr = new ArrayList<>();

        lr.add(platform1);
        lr.add(platform2);

        Consulta consulta = new Consulta();
        consulta.setResults(lr);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(consulta));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        RawgService service = retrofit.create(RawgService.class);

        //Step6: we create the call to get centers
        Call<Consulta> call = service.getConsultaPlataformas();
        // and we execute the call
        Response<Consulta> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Result> resultsResponse =response.body().getResults();
        assertFalse(resultsResponse.isEmpty());
        assertTrue(resultsResponse.size()==2);
        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert

        assertEquals(resultsResponse.get(0).getId(), platform1.getId());
        assertEquals(resultsResponse.get(0).getName(), platform1.getName());

        assertEquals(resultsResponse.get(1).getId(), platform2.getId());
        assertEquals(resultsResponse.get(1).getName(), platform2.getName());
    }

    @Test
    public void getConsultaJuegoID() throws IOException {

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(result1));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        RawgService service = retrofit.create(RawgService.class);


        //Step6: we create the call to get centers
        Call<Result> call = service.getConsultaJuegoID("1");
        // and we execute the call
        Response<Result> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        Result resultsResponse =response.body();

        assertTrue(resultsResponse != null);
        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert
        assertEquals(resultsResponse.getId(), result1.getId());
        assertEquals(resultsResponse.getName(), result1.getName());
        assertEquals(resultsResponse.getDescription(), result1.getDescription());
        assertEquals(resultsResponse.getRating(), result1.getRating());
        assertTrue(resultsResponse.getPlatforms().size()== result1.getPlatforms().size());
        assertEquals(resultsResponse.getPlatforms().get(0).getPlatform().getId(), result1.getPlatforms().get(0).getPlatform().getId());
        assertEquals(resultsResponse.getPlatforms().get(0).getPlatform().getName(), result1.getPlatforms().get(0).getPlatform().getName());
    }

    @Test
    public void getConsultaJuegos() throws IOException {

        List<Result> lr = new ArrayList<>();

        lr.add(result1);
        lr.add(result2);

        Consulta consulta = new Consulta();
        consulta.setResults(lr);

        //Step2: Define a mapper Object to JSON
        ObjectMapper objectMapper = new ObjectMapper();

        //Step3: we create a mock server, independent of the real server
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mockWebServer.url("").toString())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Step4: we create a standard response to any request (in this case, only getCenters request will be performed)
        MockResponse mockedResponse = new MockResponse();
        mockedResponse.setResponseCode(200);
        mockedResponse.setBody(objectMapper.writeValueAsString(consulta));
        mockWebServer.enqueue(mockedResponse);

        //Step5: we link the mock server with our retrofit api
        RawgService service = retrofit.create(RawgService.class);

        //Step6: we create the call to get centers
        Call<Consulta> call = service.getConsultaJuegos();
        // and we execute the call
        Response<Consulta> response = call.execute();

        //Step7: let's check that the call is executed
        assertTrue(response != null);
        assertTrue(response.isSuccessful());

        //Step8bis: check the body content
        List<Result> resultsResponse =response.body().getResults();
        assertFalse(resultsResponse.isEmpty());
        assertTrue(resultsResponse.size()==2);
        //IMPORTANT: We need to have implemented "equals" in Center class to perform this assert
        assertEquals(resultsResponse.get(0).getId(), result1.getId());
        assertEquals(resultsResponse.get(1).getId(), result2.getId());

    }
}