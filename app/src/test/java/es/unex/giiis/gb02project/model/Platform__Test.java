package es.unex.giiis.gb02project.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Platform__;

import static org.junit.Assert.*;

public class Platform__Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException{
        final Platform__ instance = new Platform__();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, new Integer(1));

        final Integer result = instance.getId();
        assertEquals("Los campos no coinciden", result, new Integer(1));
    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException{
        final Platform__ instance = new Platform__();
        Integer value = 1;
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException{
        final Platform__ instance = new Platform__();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "PS4");
        final String result = instance.getName();
        assertEquals("Los campos no coinciden", result, "PS4");
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException{
        String value = "PS4";
        final Platform__ instance = new Platform__();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getSlug() throws NoSuchFieldException, IllegalAccessException{
        final Platform__ instance = new Platform__();
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        field.set(instance, "videojuego");
        final String result = instance.getSlug();
        assertEquals("Los campos no coinciden", result, "videojuego");
    }

    @Test
    public void setSlug() throws NoSuchFieldException, IllegalAccessException{
        String value = "videojuego";
        final Platform__ instance = new Platform__();
        instance.setSlug(value);
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
}