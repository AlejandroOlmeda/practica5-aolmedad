package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Clips;

import static org.junit.Assert.assertEquals;

public class ClipsTest {

    private Clips clips;

    @Before
    public void setUpTest()throws Exception {
        clips = new Clips();
    }

    @Test
    public void get320Test() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clips.getClass().getDeclaredField("_320");
        field.setAccessible(true);
        field.set(clips, "texto");

        //when
        final String result = clips.get320();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void set320Test() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clips.set320(value);
        final Field field = clips.getClass().getDeclaredField("_320");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clips), value);
    }

    @Test
    public void get640Test() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clips.getClass().getDeclaredField("_640");
        field.setAccessible(true);
        field.set(clips, "texto");

        //when
        final String result = clips.get640();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void set640Test() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clips.set640(value);
        final Field field = clips.getClass().getDeclaredField("_640");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clips), value);
    }

    @Test
    public void getFullTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clips.getClass().getDeclaredField("full");
        field.setAccessible(true);
        field.set(clips, "texto");

        //when
        final String result = clips.getFull();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void setFullTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clips.setFull(value);
        final Field field = clips.getClass().getDeclaredField("full");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clips), value);
    }
}