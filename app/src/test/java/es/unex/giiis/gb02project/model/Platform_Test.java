package es.unex.giiis.gb02project.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Platform_;

import static org.junit.Assert.*;

public class Platform_Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, new Integer(1));

        final Integer result = instance.getId();
        assertEquals("Los campos no coinciden", result, new Integer(1));
    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Integer value = 1;
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "PS4");
        final String result = instance.getName();
        assertEquals("Los campos no coinciden", result, "PS4");
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException{
        String value = "PS4";
        final Platform_ instance = new Platform_();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getSlug() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        field.set(instance, "videojuego");
        final String result = instance.getSlug();
        assertEquals("Los campos no coinciden", result, "videojuego");
    }

    @Test
    public void setSlug() throws NoSuchFieldException, IllegalAccessException{
        String value = "videojuego";
        final Platform_ instance = new Platform_();
        instance.setSlug(value);
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getImage() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        field.set(instance, object);
        final Object result = instance.getImage();
        assertEquals("Los campos no coinciden", result, object);
    }

    @Test
    public void setImage() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        instance.setImage(object);
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), object);
    }

    @Test
    public void getYearEnd() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        final Field field = instance.getClass().getDeclaredField("yearEnd");
        field.setAccessible(true);
        field.set(instance, object);
        final Object result = instance.getYearEnd();
        assertEquals("Los campos no coinciden", result, object);
    }

    @Test
    public void setYearEnd() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        instance.setYearEnd(object);
        final Field field = instance.getClass().getDeclaredField("yearEnd");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), object);
    }

    @Test
    public void getYearStart() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        final Field field = instance.getClass().getDeclaredField("yearStart");
        field.setAccessible(true);
        field.set(instance, object);
        final Object result = instance.getYearStart();
        assertEquals("Los campos no coinciden", result, object);
    }

    @Test
    public void setYearStart() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Object object = new Object();
        instance.setYearStart(object);
        final Field field = instance.getClass().getDeclaredField("yearStart");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), object);
    }

    @Test
    public void getGamesCount() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        field.set(instance, new Integer(35));

        final Integer result = instance.getGamesCount();
        assertEquals("Los campos no coinciden", result, new Integer(35));
    }

    @Test
    public void setGamesCount() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        Integer value = 35;
        instance.setGamesCount(value);
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getImageBackground() throws NoSuchFieldException, IllegalAccessException{
        final Platform_ instance = new Platform_();
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        field.set(instance, "blanco");
        final String result = instance.getImageBackground();
        assertEquals("Los campos no coinciden", result, "blanco");
    }

    @Test
    public void setImageBackground() throws NoSuchFieldException, IllegalAccessException{
        String value = "blanco";
        final Platform_ instance = new Platform_();
        instance.setImageBackground(value);
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
}