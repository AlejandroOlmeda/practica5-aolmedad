package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Clip;
import es.unex.giiis.gb02project.dataApp.model.Clips;

import static org.junit.Assert.assertEquals;

public class ClipTest {

    private Clip clip;

    @Before
    public void setUp() throws Exception {
        clip = new Clip();
    }

    @Test
    public void getClipTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clip.getClass().getDeclaredField("clip");
        field.setAccessible(true);
        field.set(clip, "texto");

        //when
        final String result = clip.getClip();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void setClipTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clip.setClip(value);
        final Field field = clip.getClass().getDeclaredField("clip");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clip), value);
    }

    @Test
    public void getClipsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clip.getClass().getDeclaredField("clips");
        field.setAccessible(true);
        Clips value = new Clips();
        field.set(clip, value);


        //when
        final Clips result = clip.getClips();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setClipsTest() throws NoSuchFieldException, IllegalAccessException {
        Clips value = new Clips();

        clip.setClips(value);
        final Field field = clip.getClass().getDeclaredField("clips");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clip), value);
    }

    @Test
    public void getVideoTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clip.getClass().getDeclaredField("video");
        field.setAccessible(true);
        field.set(clip, "texto");

        //when
        final String result = clip.getVideo();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void setVideoTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clip.setVideo(value);
        final Field field = clip.getClass().getDeclaredField("video");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clip), value);
    }

    @Test
    public void getPreviewTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = clip.getClass().getDeclaredField("preview");
        field.setAccessible(true);
        field.set(clip, "texto");

        //when
        final String result = clip.getPreview();

        //then
        assertEquals("field wasn't retrieved properly", result, "texto");
    }

    @Test
    public void setPreviewTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        clip.setPreview(value);
        final Field field = clip.getClass().getDeclaredField("preview");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(clip), value);
    }
}