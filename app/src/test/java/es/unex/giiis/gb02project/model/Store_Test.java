package es.unex.giiis.gb02project.model;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Store_;

import static org.junit.Assert.*;

public class Store_Test {

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getId();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        Store_ instance = new Store_();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "NameExample");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", result, "NameExample");
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "NameExample";
        Store_ instance = new Store_();
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSlugTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        field.set(instance, "SlugExample");
        //when
        final String result = instance.getSlug();
        //then
        assertEquals("field wasn't retrieved properly", result, "SlugExample");
    }

    @Test
    public void setSlugTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "SlugExample";
        Store_ instance = new Store_();
        instance.setSlug(value);
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getDomainTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("domain");
        field.setAccessible(true);
        field.set(instance, "DomainExample");
        //when
        final String result = instance.getDomain();
        //then
        assertEquals("field wasn't retrieved properly", result, "DomainExample");
    }

    @Test
    public void setDomainTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "DomainExample";
        Store_ instance = new Store_();
        instance.setDomain(value);
        final Field field = instance.getClass().getDeclaredField("domain");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getGamesCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        field.set(instance, 1);
        //when
        final Integer result = instance.getGamesCount();
        //then
        assertEquals("field wasn't retrieved properly", result.intValue(), 1);
    }

    @Test
    public void setGamesCountTest() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        Store_ instance = new Store_();
        instance.setGamesCount(value);
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getImageBackgroundTest() throws NoSuchFieldException, IllegalAccessException {
        final Store_ instance = new Store_();
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        field.set(instance, "ImageBackgroundExample");
        //when
        final String result = instance.getImageBackground();
        //then
        assertEquals("field wasn't retrieved properly", result, "ImageBackgroundExample");
    }

    @Test
    public void setImageBackgroundTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "ImageBackgroundExample";
        Store_ instance = new Store_();
        instance.setImageBackground(value);
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}