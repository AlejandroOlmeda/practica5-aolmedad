package es.unex.giiis.gb02project.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.ShortScreenshot;

import static org.junit.Assert.*;

public class ShortScreenshotTest {


    @Test
    public void getId() throws NoSuchFieldException, IllegalAccessException {
        final ShortScreenshot instance = new ShortScreenshot();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, new Integer(1));

        final Integer result = instance.getId();
        assertEquals("Los campos no coinciden", result, new Integer(1));
    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException {
        final ShortScreenshot instance = new ShortScreenshot();
        Integer value = 1;
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }

    @Test
    public void getImage() throws NoSuchFieldException, IllegalAccessException {
        final ShortScreenshot instance = new ShortScreenshot();
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        field.set(instance, "Iron man");
        final String result = instance.getImage();
        assertEquals("Los campos no coinciden", result, "Iron man");
    }

    @Test
    public void setImage() throws NoSuchFieldException, IllegalAccessException {
        String value = "Iron man";
        final ShortScreenshot instance = new ShortScreenshot();
        instance.setImage(value);
        final Field field = instance.getClass().getDeclaredField("image");
        field.setAccessible(true);
        assertEquals("Los campos no coinciden", field.get(instance), value);
    }
}