package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.AddedByStatus;
import es.unex.giiis.gb02project.dataApp.model.Clip;
import es.unex.giiis.gb02project.dataApp.model.Genre;
import es.unex.giiis.gb02project.dataApp.model.ParentPlatform;
import es.unex.giiis.gb02project.dataApp.model.Platform;
import es.unex.giiis.gb02project.dataApp.model.Rating;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.model.ShortScreenshot;
import es.unex.giiis.gb02project.dataApp.model.Store;
import es.unex.giiis.gb02project.dataApp.model.Tag;

import static org.junit.Assert.assertEquals;

public class ResultTest {

    public Result result_;

    @Before
    public void setUp() throws Exception {
        result_ = new Result();
    }

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("id");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setId(value);
        final Field field = result_.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getSlugTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getSlug();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSlugTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        result_.setSlug(value);
        final Field field = result_.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getNameTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("name");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getName();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setNameTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        result_.setName(value);
        final Field field = result_.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getReleasedTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("released");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getReleased();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setReleasedTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        result_.setReleased(value);
        final Field field = result_.getClass().getDeclaredField("released");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getTbaTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("tba");
        field.setAccessible(true);
        boolean value = true;
        field.set(result_, value);

        //when
        final boolean result = result_.getTba();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setTbaTest() throws NoSuchFieldException, IllegalAccessException {
        boolean value = true;

        result_.setTba(value);
        final Field field = result_.getClass().getDeclaredField("tba");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getBackgroundImageTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("backgroundImage");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getBackgroundImage();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setBackgroundImageTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";;

        result_.setBackgroundImage(value);
        final Field field = result_.getClass().getDeclaredField("backgroundImage");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getBackgroundImageAdditionalTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("backgroundImageAdditional");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getBackgroundImageAdditional();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setBackgroundImageAdditionalTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";;

        result_.setBackgroundImageAdditional(value);
        final Field field = result_.getClass().getDeclaredField("backgroundImageAdditional");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("description");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getDescription();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setDescriptionTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";;

        result_.setDescription(value);
        final Field field = result_.getClass().getDeclaredField("description");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getRatingTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("rating");
        field.setAccessible(true);
        double value = 1.0;
        field.set(result_, value);

        //when
        final double result = result_.getRating();

        //then
        assertEquals("field wasn't retrieved properly", result, value, 0.01);
    }

    @Test
    public void setRatingTest() throws NoSuchFieldException, IllegalAccessException {
        double value = 1.0;

        result_.setRating(value);
        final Field field = result_.getClass().getDeclaredField("rating");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getRatingTopTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("ratingTop");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getRatingTop();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRatingTopTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setRatingTop(value);
        final Field field = result_.getClass().getDeclaredField("ratingTop");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getRatingsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("ratings");
        field.setAccessible(true);
        List<Rating> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<Rating> result = result_.getRatings();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRatingsTest() throws NoSuchFieldException, IllegalAccessException {
        List<Rating> value = new ArrayList<>();

        result_.setRatings(value);
        final Field field = result_.getClass().getDeclaredField("ratings");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getRatingsCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("ratingsCount");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getRatingsCount();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setRatingsCountTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setRatingsCount(value);
        final Field field = result_.getClass().getDeclaredField("ratingsCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getReviewsTextCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("reviewsTextCount");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getReviewsTextCount();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setReviewsTextCountTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setReviewsTextCount(value);
        final Field field = result_.getClass().getDeclaredField("reviewsTextCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getAddedTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("added");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getAdded();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setAddedTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setAdded(value);
        final Field field = result_.getClass().getDeclaredField("added");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getAddedByStatus() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("addedByStatus");
        field.setAccessible(true);
        AddedByStatus value = new AddedByStatus();
        field.set(result_, value);

        //when
        final AddedByStatus result = result_.getAddedByStatus();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setAddedByStatusTest() throws NoSuchFieldException, IllegalAccessException {
        AddedByStatus value = new AddedByStatus();

        result_.setAddedByStatus(value);
        final Field field = result_.getClass().getDeclaredField("addedByStatus");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getMetacriticTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("metacritic");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getMetacritic();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setMetacriticTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setMetacritic(value);
        final Field field = result_.getClass().getDeclaredField("metacritic");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getPlaytimeTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("playtime");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getPlaytime();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPlaytimeTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setPlaytime(value);
        final Field field = result_.getClass().getDeclaredField("playtime");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getSuggestionsCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("suggestionsCount");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getSuggestionsCount();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSuggestionsCountTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setSuggestionsCount(value);
        final Field field = result_.getClass().getDeclaredField("suggestionsCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getUserGameTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("userGame");
        field.setAccessible(true);
        Object value = new Object();
        field.set(result_, value);

        //when
        final Object result = result_.getUserGame();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setUserGameTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = new Object();

        result_.setUserGame(value);
        final Field field = result_.getClass().getDeclaredField("userGame");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getReviewsCountTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("reviewsCount");
        field.setAccessible(true);
        int value = 1;
        field.set(result_, value);

        //when
        final int result = result_.getReviewsCount();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setReviewsCountTest() throws NoSuchFieldException, IllegalAccessException {
        int value = 1;

        result_.setReviewsCount(value);
        final Field field = result_.getClass().getDeclaredField("reviewsCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getSaturatedColorTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("saturatedColor");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getSaturatedColor();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setSaturatedColorTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        result_.setSaturatedColor(value);
        final Field field = result_.getClass().getDeclaredField("saturatedColor");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getDominantColorTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("dominantColor");
        field.setAccessible(true);
        String value = "texto";
        field.set(result_, value);

        //when
        final String result = result_.getDominantColor();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setDominantColorTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "texto";

        result_.setDominantColor(value);
        final Field field = result_.getClass().getDeclaredField("dominantColor");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getPlatformsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("platforms");
        field.setAccessible(true);
        List<Platform> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<Platform> result = result_.getPlatforms();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setPlatformsTest() throws NoSuchFieldException, IllegalAccessException {
        List<Platform> value = new ArrayList<>();

        result_.setPlatforms(value);
        final Field field = result_.getClass().getDeclaredField("platforms");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getParentPlatformsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("parentPlatforms");
        field.setAccessible(true);
        List<ParentPlatform> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<ParentPlatform> result = result_.getParentPlatforms();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setParentPlatformsTest() throws NoSuchFieldException, IllegalAccessException {
        List<ParentPlatform> value = new ArrayList<>();

        result_.setParentPlatforms(value);
        final Field field = result_.getClass().getDeclaredField("parentPlatforms");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getGenresTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("genres");
        field.setAccessible(true);
        List<Genre> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<Genre> result = result_.getGenres();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setGenresTest() throws NoSuchFieldException, IllegalAccessException {
        List<Genre> value = new ArrayList<>();

        result_.setGenres(value);
        final Field field = result_.getClass().getDeclaredField("genres");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getStoresTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("stores");
        field.setAccessible(true);
        List<Store> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<Store> result = result_.getStores();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setStoresTest() throws NoSuchFieldException, IllegalAccessException {
        List<Store> value = new ArrayList<>();

        result_.setStores(value);
        final Field field = result_.getClass().getDeclaredField("stores");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getClipTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("clip");
        field.setAccessible(true);
        Clip value = new Clip();
        field.set(result_, value);

        //when
        final Clip result = result_.getClip();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setClipTest() throws NoSuchFieldException, IllegalAccessException {
        Clip value = new Clip();

        result_.setClip(value);
        final Field field = result_.getClass().getDeclaredField("clip");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getTagsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("tags");
        field.setAccessible(true);
        List<Tag> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<Tag> result = result_.getTags();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setTagsTest() throws NoSuchFieldException, IllegalAccessException {
        List<Tag> value = new ArrayList<>();

        result_.setTags(value);
        final Field field = result_.getClass().getDeclaredField("tags");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }

    @Test
    public void getShortScreenshotsTest() throws NoSuchFieldException, IllegalAccessException {
        final Field field = result_.getClass().getDeclaredField("shortScreenshots");
        field.setAccessible(true);
        List<ShortScreenshot> value = new ArrayList<>();
        field.set(result_, value);

        //when
        final List<ShortScreenshot> result = result_.getShortScreenshots();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void setShortScreenshotsTest() throws NoSuchFieldException, IllegalAccessException {
        List<ShortScreenshot> value = new ArrayList<>();

        result_.setShortScreenshots(value);
        final Field field = result_.getClass().getDeclaredField("shortScreenshots");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(result_), value);
    }
}