package es.unex.giiis.gb02project.network;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.gb02project.dataApp.model.Platform_;
import es.unex.giiis.gb02project.dataApp.model.Result;
import es.unex.giiis.gb02project.dataApp.network.ResultNetworkDataSource;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.ImagesGame;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.NewGames;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.Platform;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PlatformCategory;
import es.unex.giiis.gb02project.dataApp.roomdb.entities.PopularGames;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.when;

import static org.junit.Assert.*;

public class ResultNetworkDataSourceTest {

    @Mock
    private ResultNetworkDataSource resultNetworkDataSource;

    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    @Before
    public void setUp() throws Exception {
        resultNetworkDataSource = ResultNetworkDataSource.getInstance();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getInstance() {
        assertNotNull(resultNetworkDataSource);
    }

    @Test
    public void shouldGetCurrentSearchGames() {
        final MutableLiveData<List<Result>> mDownloadedSearch = new MutableLiveData<>();
        List<Result> rl=new ArrayList<>();

        Result result1 = new Result();
        result1.setId(1);
        result1.setName("Juego de prueba");
        result1.setDescription("Descripción de prueba");
        result1.setRating(4.2);

        es.unex.giiis.gb02project.dataApp.model.Platform p1 = new es.unex.giiis.gb02project.dataApp.model.Platform();
        Platform_ p_1 = new Platform_();
        p_1.setId(4);
        p_1.setName("Aventura");
        p1.setPlatform(p_1);
        List<es.unex.giiis.gb02project.dataApp.model.Platform> lp = new ArrayList<>();
        lp.add(p1);
        result1.setPlatforms(lp);



        Result result2 = new Result();
        result2.setId(2);
        result2.setName("Juego de prueba 2");
        result2.setDescription("Descripción de prueba 2");
        result2.setRating(4.2);
        es.unex.giiis.gb02project.dataApp.model.Platform p2 = new es.unex.giiis.gb02project.dataApp.model.Platform();
        Platform_ p_2 = new Platform_();
        p_2.setId(5);
        p_2.setName("Acción");
        p2.setPlatform(p_2);
        List<es.unex.giiis.gb02project.dataApp.model.Platform> lp2 = new ArrayList<>();
        lp2.add(p2);
        result2.setPlatforms(lp2);

        rl.add(result1);
        rl.add(result2);

        mDownloadedSearch.setValue(rl);

        LiveData<List<Result>> ldsg = mDownloadedSearch;

        when(resultNetworkDataSource.getCurrentSearchGames()).thenReturn(ldsg);

        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getId(), result1.getId());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getName(), result1.getName());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getDescription(), result1.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getRating(), result1.getRating());
        assertTrue(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getPlatforms().size()== result1.getPlatforms().size());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getPlatforms().get(0).getPlatform().getId(), result1.getPlatforms().get(0).getPlatform().getId());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(0).getPlatforms().get(0).getPlatform().getName(), result1.getPlatforms().get(0).getPlatform().getName());


        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getId(), result2.getId());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getName(), result2.getName());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getDescription(), result2.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getRating(), result2.getRating());
        assertTrue(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getPlatforms().size()== result2.getPlatforms().size());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getPlatforms().get(0).getPlatform().getId(), result2.getPlatforms().get(0).getPlatform().getId());
        assertEquals(resultNetworkDataSource.getCurrentSearchGames().getValue().get(1).getPlatforms().get(0).getPlatform().getName(), result2.getPlatforms().get(0).getPlatform().getName());

    }

    @Test
    public void shouldGetCurrentImages() {
        final MutableLiveData<List<ImagesGame>> mDownloadedImage = new MutableLiveData<>();
        List<ImagesGame> pl=new ArrayList<>();

        ImagesGame ig1 = new ImagesGame(1, 1, "img 1");
        ImagesGame ig2 = new ImagesGame(2, 2, "img 2");

        pl.add(ig1);
        pl.add(ig2);

        mDownloadedImage.setValue(pl);

        LiveData<List<ImagesGame>> ldpc = mDownloadedImage;

        when(resultNetworkDataSource.getCurrentImages()).thenReturn(ldpc);

        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(0).getID(), ig1.getID());
        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(0).getIDGame(), ig1.getIDGame());
        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(0).getImage(), ig1.getImage());


        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(1).getID(), ig2.getID());
        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(1).getIDGame(), ig2.getIDGame());
        assertEquals(resultNetworkDataSource.getCurrentImages().getValue().get(1).getImage(), ig2.getImage());
    }

    @Test
    public void shouldGetCurrentPlatforms() {
        final MutableLiveData<List<Platform>> mDownloadedPlatform = new MutableLiveData<>();
        List<Platform> pl=new ArrayList<>();

        Platform pc1 = new Platform(1,1, "plataforma 1", Platform.TipoPopular);
        Platform pc2 = new Platform(2,2, "plataforma 2", Platform.TipoNovedad);

        pl.add(pc1);
        pl.add(pc2);

        mDownloadedPlatform.setValue(pl);

        LiveData<List<Platform>> ldpc = mDownloadedPlatform;

        when(resultNetworkDataSource.getCurrentPlatforms()).thenReturn(ldpc);

        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(0).getIDPlatform(), pc1.getIDPlatform());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(0).getIDGame(), pc1.getIDGame());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(0).getName(), pc1.getName());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(0).getType(), pc1.getType());


        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(1).getIDPlatform(), pc2.getIDPlatform());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(1).getIDGame(), pc2.getIDGame());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(1).getName(), pc2.getName());
        assertEquals(resultNetworkDataSource.getCurrentPlatforms().getValue().get(1).getType(), pc2.getType());
    }


    @Test
    public void shouldGetCurrentNewGames() {
        final MutableLiveData<NewGames[]> mDownloadedNewGames = new MutableLiveData<>();
        NewGames[] nga=new NewGames[2];

        NewGames ng1 = new NewGames(1, "game1", "img 1", 3.1, "description 1", "img a 1");
        NewGames ng2 = new NewGames(2, "game2", "img 2", 2.6, "description 2", "img a 2");

        nga[0] = ng1;
        nga[1] = ng2;

        mDownloadedNewGames.setValue(nga);

        LiveData<NewGames[]> ldng = mDownloadedNewGames;

        when(resultNetworkDataSource.getCurrentNewGames()).thenReturn(ldng);

        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getID(), ng1.getID());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getName(), ng1.getName());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getBackgroundImage(), ng1.getBackgroundImage());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getRating(), ng1.getRating());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getDescription(), ng1.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[0].getBackground_image_additional(), ng1.getBackground_image_additional());

        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getID(), ng2.getID());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getName(), ng2.getName());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getBackgroundImage(), ng2.getBackgroundImage());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getRating(), ng2.getRating());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getDescription(), ng2.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentNewGames().getValue()[1].getBackground_image_additional(), ng2.getBackground_image_additional());
    }

    @Test
    public void shouldGetCurrentPopularGames() {
        final MutableLiveData<PopularGames[]> mDownloadedPopularGames = new MutableLiveData<>();
        PopularGames[] pga=new PopularGames[2];

        PopularGames pg1 = new PopularGames(1, "game1", "img 1", 3.1, "description 1", "img a 1");
        PopularGames pg2 = new PopularGames(2, "game2", "img 2", 2.6, "description 2", "img a 2");

        pga[0] = pg1;
        pga[1] = pg2;

        mDownloadedPopularGames.setValue(pga);

        LiveData<PopularGames[]> ldpg = mDownloadedPopularGames;

        when(resultNetworkDataSource.getCurrentPopularGames()).thenReturn(ldpg);

        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getID(), pg1.getID());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getName(), pg1.getName());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getBackgroundImage(), pg1.getBackgroundImage());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getRating(), pg1.getRating());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getDescription(), pg1.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[0].getBackground_image_additional(), pg1.getBackground_image_additional());

        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getID(), pg2.getID());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getName(), pg2.getName());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getBackgroundImage(), pg2.getBackgroundImage());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getRating(), pg2.getRating());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getDescription(), pg2.getDescription());
        assertEquals(resultNetworkDataSource.getCurrentPopularGames().getValue()[1].getBackground_image_additional(), pg2.getBackground_image_additional());

    }

}