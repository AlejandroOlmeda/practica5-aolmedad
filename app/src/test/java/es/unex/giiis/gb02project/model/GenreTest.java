package es.unex.giiis.gb02project.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giiis.gb02project.dataApp.model.Genre;

import static org.junit.Assert.*;

public class GenreTest {

    private Genre instance;

    @Before
    public void setUp() throws Exception {
        instance = new Genre();
    }

    @Test
    public void getId() throws IllegalAccessException, NoSuchFieldException {
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        //when
        final Integer result = instance.getId();
        //then
        assertEquals("field wasn't retrieved properly", result, new Integer(1));
    }

    @Test
    public void setId() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getName() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getName();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void setName() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setName(value);
        final Field field = instance.getClass().getDeclaredField("name");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSlug() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getSlug();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void setSlug() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setSlug(value);
        final Field field = instance.getClass().getDeclaredField("slug");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getGamesCount() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        field.set(instance, new Integer(1));
        //when
        final Integer result = instance.getGamesCount();
        //then
        assertEquals("field wasn't retrieved properly", result, new Integer(1));
    }

    @Test
    public void setGamesCount() throws NoSuchFieldException, IllegalAccessException {
        Integer value = 1;
        instance.setGamesCount(value);
        final Field field = instance.getClass().getDeclaredField("gamesCount");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getImageBackground() throws NoSuchFieldException, IllegalAccessException {
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        field.set(instance, "text");
        //when
        final String result = instance.getImageBackground();
        //then
        assertEquals("field wasn't retrieved properly", result, "text");
    }

    @Test
    public void setImageBackground() throws NoSuchFieldException, IllegalAccessException {
        String value = "text";
        instance.setImageBackground(value);
        final Field field = instance.getClass().getDeclaredField("imageBackground");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }
}